cmake_minimum_required(VERSION 3.10)

project(heh)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ../lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ../lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ../bin)

include_directories(include)

file(GLOB SOURCES "src/*.cpp")
SET(CMAKE_CXX_FLAGS "-Wall -std=c++17 -Wextra -g")
set(EXECUTABLE_NAME "heh")
add_executable(${EXECUTABLE_NAME} ${SOURCES})

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})