#ifndef DEFINITIONS_HPP
#define DEFINITONS_HPP
#include <iostream>
#include <string>

template <typename T>
T add(T a, T b)
{
    return a + b;
}

void xd();

class foo
{
public:
    foo(std::string& s) { std::cout << s << '\n'; }
    int x = 0;
};

#endif